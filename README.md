# SpotOn Technical Challenge
Program take input of a single activity URL and returns other activity URLs from the same root domain. User may specify the desired number of resulting URLs.

**NOTE:** program will only return as many active URLs it can find, up until the desired number specified by the user

## **Via Web API**
-----------
The live API is hosted on Heroku. To access it, use the following web request URL:
```
http://desolate-caverns-7299.herokuapp.com/activities/?path=[ACTIVITY_URL]&count=[URL_COUNT]
```
where ```[ACTIVITY_URL]``` is the URL of the original activity page and ```[URL_COUNT]``` is the desired number of activity URLs you'd like in return. The API returns JSON data which includes the found activity URLs, the final count of found activity URLS, and the original activity URL.

To run this via command line, use the following in your command line tool of choice:
```
$ curl http://desolate-caverns-7299.herokuapp.com/activities/?path=[ACTIVITY_URL]&count=[URL_COUNT]
```
...

**Example:**
```
http://desolate-caverns-7299.herokuapp.com/activities/?path=http://events.stanford.edu/events/353/35309/&count=10
```
**Result:**
```
{
  "activity_urls": [
    "http://events.stanford.edu/events/353/35311", 
    "http://events.stanford.edu/events/353/35313", 
    "http://events.stanford.edu/events/353/35315", 
    "http://events.stanford.edu/events/353/35317", 
    "http://events.stanford.edu/events/353/35319", 
    "http://events.stanford.edu/events/353/35321", 
    "http://events.stanford.edu/events/353/35323", 
    "http://events.stanford.edu/events/353/35325", 
    "http://events.stanford.edu/events/353/35327", 
    "http://events.stanford.edu/events/353/35329"
  ], 
  "count": 10, 
  "original_url": "http://events.stanford.edu/events/353/35309/"
}
```

**KNOWN ISSUES**

* API currently cannot take in any URLs with their own parameters (e.g. http://some-domain.com/?id=123&var=unfortunate)

## **Via Command Line**
--------------------
To access the full functionality of the program, you can use the command line to run the Python code.

1. Clone this repository.
2. Open a command line tool of your choice (e.g. Terminal in Mac)
3. Go to the directory containing code for this project.
4. In the root folder, which contains ```index.py```, run the following:

```
$ python index.py [ACTIVITY_URL] [COUNT]
```
where ```[ACTIVITY_URL]``` is the URL of the original activity page and ```[COUNT]``` is the desired number of URLs to be returned.

...

**Example:**
```
$ python index.py http://events.stanford.edu/events/353/35309/ 2
```
**Result:**
```
{"count": 2, "activity_urls": ["http://events.stanford.edu/events/353/35311", "http://events.stanford.edu/events/353/35313"], "original_url": "http://events.stanford.edu/events/353/35309/"} 
```

**KNOWN ISSUES**

* Unable to determine whether Wordpress or similar pages return empty activity pages (e.g. http://www.workshopsf.org/?page_id=140&id=1328)
* For pages not using numerical IDs, difficult to find which crawled URLs are events or activities (e.g. http://calendar.boston.com/lowell_ma/events/show/274127485-mrt-presents-shakespeares-will)