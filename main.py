import urllib
import re

# Timeout multiplier to determine how far ahead program should look when
#   finding future events using incremental strategy
TIMEOUT_CAPP_MULTIPLIER = 3

def is_number(text):
	try:
		float(text)
		return True
	except ValueError:
		return False

def build_url(url_sections, event_id, delimeter):
	return '/'.join(url_sections) + delimeter + str(event_id)

def find_pattern(url, regex, scope='all_results'):
	htmlfile = urllib.urlopen(url)
	htmltext = htmlfile.read()
	pattern = re.compile(regex)
	if scope == 'first':
		return re.search(pattern, htmltext)
	else:
		return re.findall(pattern, htmltext)

def page_exists(url):
	title_regex = '<title>(.+?)</title>'
	title = find_pattern(url, title_regex, "first")
	if title is not None and '404' in title.group():
		return False
	else:
		return True

def get_activity_urls(event_id, num_of_urls, activities, delimeter, url_sections):
	# Using timeout_capp to determine a max event_id number before quiting to
	#   prevent infinitely incrementing event_id while looking for activities
	timeout_capp = event_id + num_of_urls * TIMEOUT_CAPP_MULTIPLIER

	while len(activities) < num_of_urls and event_id < timeout_capp:
		event_id += 1
		new_activity = build_url(url_sections, event_id, delimeter)
		if page_exists(new_activity):
			activities.append(new_activity)
	return activities

def get_nested_activitiy_urls(url_sections, activities, num_of_urls):
	url_sections.pop()
	url_sections.pop()
	directory = '/'.join(url_sections)
	link_regex = '<a href="(.+?)">'

	if page_exists(directory):
		found_links = find_pattern(directory, link_regex)
		i = 0
		while i < len(found_links) and len(activities) < num_of_urls:
			if found_links[i].startswith('/'):
				activities.append(directory + found_links[i])
			i += 1

		return activities

def find_activities(url, num_of_activities):
	activities = []
	url_sections = url.split('/')
	if url_sections[len(url_sections)-1] == '':
		url_sections.pop()

	event_id = url_sections[len(url_sections)-1]

	if is_number(event_id):
		new_event_id = int(event_id)
		url_sections.pop()
		get_activity_urls(new_event_id, num_of_activities, activities, '/', url_sections)

	else:
		if '=' in event_id:
			url_sections.pop()
			split_event_id_params = event_id.split('=')
			param_event_id = split_event_id_params[len(split_event_id_params)-1]

			split_event_id_params.pop()
			remade_event_id = '='.join(split_event_id_params)
			url_sections.append(remade_event_id)

			new_event_id = int(param_event_id)
			activities = get_activity_urls(new_event_id, num_of_activities, activities, '=', url_sections)

		else:
			activities = get_nested_activitiy_urls(url_sections, activities, num_of_activities)

	return activities