import main
import sys
import json

def create_json(original_url, count):
	activities_found = main.find_activities(original_url, int(count))
	
	data = {}
	data['original_url'] = original_url
	data['count'] = len(activities_found)
	data['activity_urls'] = activities_found
	return json.dumps(data)

print create_json(sys.argv[1], sys.argv[2])